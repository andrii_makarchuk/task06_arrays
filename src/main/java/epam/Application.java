package epam;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Application {
    private static Random random = new Random();
    public static void main(String[] args) {
        AutoCloseable closeable;
        Integer[] integers = new Integer[100];
        randomAdd(integers);
        show(integers);
        Arrays.sort(integers);
        System.out.println();
        show(integers);
        List list = new LinkedList();
    }

    public static void randomAdd(Integer[] a){
        for (int i = 0; i < a.length; i++) {
            a[i] = random.nextInt(1000);
        }
    }
    public static void show(Integer[] a){
        for (int i = 0; i < a.length; i++) {
            System.out.print(a[i] + " ");
        }
    }
}
