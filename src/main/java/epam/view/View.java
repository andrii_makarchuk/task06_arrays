package epam.view;

import com.epam.controller.Controller;
import com.epam.controller.ControllerImp;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class View {
    private Controller controller;
    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner scanner = new Scanner(System.in);

    public View() {
        controller = new ControllerImp();
        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - Start game");
        menu.put("2", "  2 - Generate randoms arrays");
        menu.put("3", "  2 - Generate randoms arrays");
        menu.put("4", "  4 - Show arrays");
        menu.put("5", "  5 - Generate third array A");
        menu.put("6", "  6 - Generate third array B");
        menu.put("7", "  7 - Delete all repetitive numbers from first array");
        menu.put("8", "  8 - Delete repetitive numbers from second array which ");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
    }

    private void pressButton1() {

    }

    private void pressButton2() {

    }

    private void pressButton3() {

    }

    private void pressButton4() {

    }

    private void pressButton5() {

    }

    private void pressButton6() {

    }

    private void pressButton7() {

    }

    private void outputMenu() {
        System.out.println("Menu: ");
        for (String str : menu.values()
        ) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.print("Press button: ");
            keyMenu = scanner.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {

            }
        } while (!keyMenu.equals("Q"));
    }
}
