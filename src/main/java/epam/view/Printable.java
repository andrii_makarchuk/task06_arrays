package epam.view;

@FunctionalInterface
public interface Printable {
    void print();
}
